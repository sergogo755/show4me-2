import Header from './components/Header';
import './App.css';
import Saves from './components/Saves';
import React, { useEffect,useState } from 'react';
import Form from './components/Form';
import TodoList from './components/TodoList';

const App = () => {


  //const initialState = JSON.parse(localStorage.getItem("todos")) || []
  const [input,setInput] = useState("");
  const [todos,setTodos] = useState([]) // initialState
  const [editTodo, setEditTodo]= useState(null)
  const [currentPage, setCurrentPage] = useState(1)
  const [values] = useState(5 )

  useEffect(()=>{
    localStorage.setItem("todos",JSON.stringify(todos))
    console.log(todos.length);
  },[todos])

  return (
    <div className='container'>
      <div className='app-wrapper'>
        <div>
          <Header/>
        </div>
        <div>
          <Form 
          input={input}
          setInput={setInput}
          todos={todos}
          setTodos={setTodos}
          editTodo={editTodo}
          setEditTodo={setEditTodo}
          />
        </div>
        <div>
          <TodoList
          todos={todos}
          setTodos={setTodos}
          setEditTodo={setEditTodo}/>
        </div>
        <div><Saves
          todos={todos}
          setTodos={setTodos}
          /></div>
      </div>
    </div>
  );
}

export default App;
