import React,{useEffect} from "react";
import {v4 as uuidv4} from "uuid"

const Form =({input,setInput,todos,setTodos,editTodo,setEditTodo })=>{

    const updateTodo = ({title,id,completed}) =>{
        const newTodo = todos.map((todo) =>
            todo.id === id ? {title,id,completed} : todo   //берёт значение инпута и меняет его
        );
        setTodos(newTodo);
        setEditTodo("") //очищает инпут
    }
    const onFormSubmit =(event) => {
        event.preventDefault();
        if(!editTodo){
            setTodos([...todos,{id: uuidv4(), title:input, completed:false }])  //создает todo
            setInput(""); //очищает инпут
        } else{
            
            updateTodo({title: input,id: editTodo.id,completed: editTodo.completed})
        }
    }
    


    useEffect(() =>{
        if(editTodo){
            setInput(editTodo.title)        //при клике на edit заполняет область значением инпута
        }else{
            setInput('')                    //очищает инпут
        }
    },[editTodo])

    const onInputChange = (event) =>{
        setInput(event.target.value)
    }   


    return(
        <form onSubmit={onFormSubmit}>
            <input type="text" placeholder="Enter here" className="task-input" value={input} required onChange={onInputChange}/>
            <button className="button-add" type="submit">
                {editTodo ? "OK" : "Add"}
            </button>
        </form>
    )
}

export default Form 