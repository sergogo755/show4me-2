import React from "react";


const  TodoList = ({todos,setTodos, setEditTodo}) =>{

    const handleDelete = ({id}) => {
        setTodos(todos.filter((todo) => todo.id !== id)) //удаление
    }

    const handleComplete = (todo) =>{  //Проверка сделан ли он
        setTodos(todos.map((item) =>{
            if(item.id === todo.id){   // error was here!!! =
                return {...item, completed: !item.completed}
            }
            return item; 
        }))
    }

    const handleEdit = ({id}) =>{
        const findTodo = todos.find((todo) => todo.id === id) //изменить
        setEditTodo(findTodo); 
    }

    return (
        <div >
            {todos.map((todo)=>(
                <li className="list-item" key={todo.id}>
                    <input type='text' value={todo.title} className={`list ${todo.completed ? 'complete' : ""}`} onChange={(event)=> event.preventDefault()}/>
                    <div className="buttons">
                        <button className="button-complete btn" onClick={() => handleComplete(todo)}>
                            <img src="./img/done.svg" alt=":(" />
                        </button>
                        <button className="button-edit btn" onClick={() => handleEdit(todo)}>
                            <img src="./img/change.svg" alt=":(" />
                        </button>
                        <button className="button-delete btn" onClick={() => handleDelete(todo)}>
                            <img src="./img/delete.svg" alt=":(" />
                        </button>
                    </div>
                </li>
            ))}
        </div>
    )
}

export default TodoList;